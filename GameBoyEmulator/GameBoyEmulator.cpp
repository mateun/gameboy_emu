// GameBoyEmulator.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <SDL.h>

const int W = 800, H = 600;

void clearVidmem(char* vidMem) { memset(vidMem, 0, W*H); }
void setPixel(char* vidmem, int x, int y) { vidmem[x + y * W] = 1; }



int main(int argc, char* args[])
{
	// VM stuff
	// Video memory for 320x200 resolution
	char videoMemory[W*H]; clearVidmem(videoMemory);
	char codeMem[0xFF]; memset(codeMem, 0, 0xFF);

	
	// SDL stuff
	SDL_Window* window; SDL_Renderer* renderer; SDL_Event event;
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		SDL_Log("error at SDL init"); return 1;
	}
	if (SDL_CreateWindowAndRenderer(W, H, SDL_WINDOW_SHOWN, &window, &renderer)) {
		SDL_Log("error creating the window & renderer", SDL_GetError()); return 1;
	}
	while (1) {
		SDL_PollEvent(&event);
		if (event.type == SDL_QUIT) { break; }
		SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xff);
		SDL_RenderClear(renderer);
		SDL_SetRenderDrawColor(renderer, 0xff, 0x00, 0x11, 0xff);
		for (int i = 0; i < W*H; i++) { 
			if (videoMemory[i]) SDL_RenderDrawPoint(renderer, i % W, i/W); }
		SDL_RenderPresent(renderer);
	}

	SDL_Quit();
	return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
